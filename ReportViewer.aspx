﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ReportViewer.aspx.cs" Inherits="Simplexity.AsTrans.CrystalReportsViewer.ReportViewer" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
		<script src='<%=ResolveUrl("~/crystalreportviewers13/js/crviewer/crv.js")%>' type="text/javascript"></script>
    <script type="text/javascript">
    function resizeWindow() 
    {           
    // you can get height and width from serverside as well      
    var width=800;
    var height=600; 
    window.resizeTo(width,height);           
    }
</script>
</head>
<body onload="resizeWindow()">
    <form id="form1" runat="server">
    <div>
    <CR:CrystalReportViewer ID="CrystalReportViewer1" runat="server" 
            AutoDataBind="True" ReportSourceID="CrystalReportSource1" HasCrystalLogo="False" 
                 HasToggleGroupTreeButton="False" EnableDatabaseLogonPrompt="False"
                 ToolbarStyle-BorderStyle="None" ToolPanelView="None" Width="100%"
                 DisplayStatusbar="False" PrintMode="ActiveX" 
        HasToggleParameterPanelButton="False" EnableDrillDown="False" />
        <CR:CrystalReportSource ID="CrystalReportSource1" runat="server" />
        <asp:Label ID="lblMessage" runat="server" Text="" ForeColor="SALMON"/>
    </div>
    
    </form>
</body>

</html>
