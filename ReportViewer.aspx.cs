﻿using System;
using System.Configuration;
using System.Text;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace Simplexity.AsTrans.CrystalReportsViewer
{
	public partial class ReportViewer : System.Web.UI.Page
	{
		protected void Page_Init(object sender, EventArgs e)
		{
			try
			{
				// nombre del reporte
				string reportName = Request.QueryString["ReportName"];
				// Id del registro de la tabla principal
				string reportParamId1 = Request.QueryString["strID1"];
				//si es borrador = 1 o original = 0
				string reportAction = Request.QueryString["Action"];
				// Copias del reporte
				string reportCopies = Request.QueryString["strRPCID"];

				var message = new StringBuilder();
				if (string.IsNullOrEmpty(reportName))
				{
					message.AppendLine("ReportName,");
				}
				if (string.IsNullOrEmpty(reportParamId1))
				{
					message.AppendLine("strID1,");
				}
				if (string.IsNullOrEmpty(reportAction))
				{
					message.AppendLine("Action,");
				}
				if (string.IsNullOrEmpty(reportCopies))
				{
					message.AppendLine("strRPCID,");
				}

				if (message.Length > 0)
				{
					lblMessage.Text = message.ToString().Remove(message.ToString().LastIndexOf(','));
					return;
				}

				reportParamId1 = reportParamId1.Replace("|", ",");
				reportCopies = reportCopies.Replace("|", ",");

				reportName = ConfigurationManager.AppSettings["crystalReportPath"] + reportName;


				CrystalReportSource1.Report.FileName = reportName;
				var parametersReport = CrystalReportViewer1.ParameterFieldInfo;
				CrystalReportSource1.ReportDocument.Load(reportName);

				//Conexion a la base de datos
				var crConnection = new ConnectionInfo
				{
					UserID = ConfigurationManager.AppSettings["sqlUser"],
					ServerName = ConfigurationManager.AppSettings["sqlInstance"],
					DatabaseName = ConfigurationManager.AppSettings["sqlDatabase"],
					Password = ConfigurationManager.AppSettings["sqlPassword"]
				};
				
				AssignConnectionInfo(CrystalReportSource1.ReportDocument, crConnection);

				//Asignacion de parametros

				foreach (var parameter in parametersReport)
				{
					var parameterField = ((ParameterField) parameter);

					switch (parameterField.ParameterFieldName)
					{
						case "ID1":
							CrystalReportSource1.ReportDocument.SetParameterValue("ID1", reportParamId1);
							break;
						case "Action":
							CrystalReportSource1.ReportDocument.SetParameterValue("Action", reportAction);
							break;
						case "RPCID":
							CrystalReportSource1.ReportDocument.SetParameterValue("RPCID", reportCopies);
							break;
					}
				}

				CrystalReportViewer1.ReportSource = CrystalReportSource1.ReportDocument;
				CrystalReportViewer1.DataBind();
			}
			catch (Exception ex)
			{
				ExceptionPolicy.HandleException(ex, "LoggingReplaceException");
			}
		}

		private void AssignConnectionInfo(ReportDocument document, ConnectionInfo crConnection)
		{
			foreach (Table table in document.Database.Tables)
			{
				TableLogOnInfo logOnInfo = table.LogOnInfo;
				if (logOnInfo != null)
				{
					table.LogOnInfo.TableName = table.Name;
					table.LogOnInfo.ConnectionInfo.UserID = crConnection.UserID;
					table.LogOnInfo.ConnectionInfo.Password = crConnection.Password;
					table.LogOnInfo.ConnectionInfo.DatabaseName = crConnection.DatabaseName;
					table.LogOnInfo.ConnectionInfo.ServerName = crConnection.ServerName;
					table.ApplyLogOnInfo(table.LogOnInfo);

					CrystalReportViewer1.LogOnInfo.Add(table.LogOnInfo);
				}
			}
		}
	}
}